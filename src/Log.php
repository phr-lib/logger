<?php

namespace Phr\Logger;

use Phr\Logger\LogBase\LogBase;
use Phr\Logger\LogBase\ILogFiles;
use Phr\Logger\LogBase\ILogChars;


class Log extends LogBase
{
    public static function system( string $_content_to_log ): void 
    {
        $FileName = ILogFiles::SYSTEM_LOG;

        $FullFilePath = self::$filePath.'/'.ILogFiles::SYSTEM_LOG . '.' . self::$fileExtention;

        $FileContent = ILogChars::START_LINE 
                        .self::createLogDate(). ' // '
                        .$_content_to_log . '  |' 
                        .self::$endLine;

        file_put_contents($FullFilePath, $FileContent, FILE_APPEND | LOCK_EX);
    }
}