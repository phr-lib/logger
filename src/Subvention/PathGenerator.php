<?php

namespace Phr\Logger\Subvention;

class PathGenerator 
{
    public static function generate( string $filePath): void 
    {   
            if(!is_dir($filePath))
                    mkdir($filePath, 0777, true);
    }
}