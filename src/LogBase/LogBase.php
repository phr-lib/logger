<?php

namespace Phr\Logger\LogBase;

use Phr\Logger\Subvention\PathGenerator;
use  Phr\Logger\LogBase\ILogFilePath;

abstract class LogBase 
{   
    /**
     * 
     * @access private
     * @var loggerConfig
     * Configuration from app config
     * 
     */
    protected static $loggerConfig; 

    protected static $filePath;

    protected static $fileExtention = 'log';

    protected static $endLine = "<<< \n";


    protected static function createLogDate(): string 
    {
        return date("d.M.Y h:i:s", time());
    }

    # CONSTRUCTOR ***
    public function __construct( string $_app_config_path )
    {
        $config = json_decode( file_get_contents($_app_config_path) );

        self::$loggerConfig = $config->logger;

        self::checkOrCreatePath();
    }

    /**
     * 
     * @access private
     * @method checkOrCreatePath
     * 
     */
    private static function checkOrCreatePath(): void
    {   
        $FullPath = self::$loggerConfig->archiveRoot
                    .ILogFilePath::YEAR_LOG
                    .date("Y") . ILogFilePath::MONTH_LOG
                    .date("M");
        
        PathGenerator::generate( $FullPath );

        self::$filePath = $FullPath;
    }

    
}