<?php

namespace Phr\Logger\LogBase;

interface ILogFilePath 
{
    public const YEAR_LOG = '/year_';

    public const MONTH_LOG = '/month_';

}